# This file should wrap all playbooks to completely configure each machine from strt to finish
ansible-playbook -i hosts deploy/install_apt_packages.yaml
ansible-playbook -i hosts deploy/install_anaconda.yaml
ansible-playbook -i hosts deploy/install_conda_envs.yaml
ansible-playbook -i hosts deploy/install_post_configure.yaml
