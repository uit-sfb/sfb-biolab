# UiT SfB Biolab Azure Ansible Playbooks Setup
This repository contains code related to control and deployment of cloud-infrastructure for KJE-3604 and related SfB UiT Bioinformatics courses using Ansible Playbooks.

Ansible-Galaxy Roles required:
evandam.conda
(Install with **ansible-galaxy install evandam.conda**)

## Complete Biolab deployment
The ***deployment_wrapper.sh*** script wraps all individual deployment scripts below in the correct order.  Useful for a full course setup from scratch.  

**Before deploying, make sure that:**

 - The hosts file includes all relevant Virtual machiness already setup using Azure. Edit this file as you see fit. Use **hosts:biolab** for all machines and **hosts:template** for testing on one
 - Your ssh-key is present in authorised_keys for kursadm on every virtual machine
 - If you have a passphrase on you ssh-key, put it into ssh-agent using ssh-add <path_to_key>, or else you will have to type it one gazillion times manually during deploy.

 Usage: **`./deployment_wrapper.sh`**

### Individual deployment scripts

**install_anaconda.yaml** - Installs miniconda3 in /opt and configures repos and channels as needed
Usage: **`ansible-playbook -i hosts install_anaconda.yaml`**  
**install_conda_envs.yaml** - Creates/installs all Conda environments needed
Usage: **`ansible-playbook -i hosts create_conda_envs.yaml`**  
**install_apt_packages.yaml** - Installs all aptitude packages needed
Usage: **`ansible-playbook -i hosts install_apt_packages.yaml`**  
**install_odd_men.yaml** - Installs any tool, database etc. not present in aptitude or Conda repositories or other special cases.
Usage: **`ansible-playbook -i hosts install_odd_men.yaml`**  
**post_configure.yaml** - Sets correct owners and premissions to certain directories and files
Usage: **`ansible-playbook -i hosts post_configure.yaml`**  

## Utility scripts
Utility scripts need certain bash variables set for your specific cloud deployment, namely the web hook url, start and stop tokens. These should be set in the utility/configure.sh script and then run:
 **`source configure.sh`**  

**status.sh** - Pings all VMs to check which ones are turned on / off
Usage: **`./status.sh`**   

**start_all.sh** - Starts all VMs.
Usage: **`./start_all_vms.sh`**   

**stop_all.sh** - Stops all VMs.
Usage: **`./stop_all.sh`**   

**restart_one.sh** - Restarts one particular VM. Usefull if f.ex the VNC service has a bad day. Provide the hostname as an argument. (Also, you might want to run this in the background by appending **&** as it takes 120 seconds minimum)
Usage: **`./restart_one.sh hostname`**

**send_msg.sh** - Sends an announcement to all logged in terminals on all VMs. Useful f.ex when shutting down or updating something
Usage: **`./send_msg.sh 'Your important message goes here'`**
