# Usage: ./restart_one.sh "HOSTNAME"
wait=120
printf "Restarting $1\n"
curl -d $1 -X POST $azure_webhook_url/webhooks?token=$azure_stop_token
printf "\nWaiting for $wait seconds before post startup webhook..."
sleep $wait
printf "Startup $1\n"
curl -d $1 -X POST $azure_webhook_url/webhooks?token=$azure_start_token
printf "\n$1 will be up and running soon\n"
